<?php

declare(strict_types=1);

interface Response {
    public function statusCode(): int;
    public function payload(): array;
}

class Ok implements Response {
    public function statusCode(): int
    {
        return 200;
    }

    public function payload(): array
    {
        return ['Ok'];
    }
}

class Migration {
    public function up(): string
    {
        /**
         *  Метод должен вернуть SQL запрос на создание таблиц в БД
         */

        return 'CREATE TABLE ...';
    }
}

class Controller
{
    public function actionSave(): Response
    {
        /**
         *  В API ручке https://run.mocky.io/v3/d8e23bc9-1042-4741-84d0-506acd71fb8e
         *  есть информация о заказе с его содержимым.
         *  Задача: нужно получить данные из API и сохранить их в MySQL.
         *  Метод может быть вызван повторно, в ответе от API заказ и его состав может измениться.
         *
         *  Полностью реализацию писать не обязательно, можно оставить заглушки с комментариями.
         *  Задача на проектирование БД, использование принципов SOLID, обработку запроса и этапов сохранения данных.
         */

        return new Ok();
    }
}

class TestCase {
    public function testSuccessful(): void
    {
        $result = (new Controller())->actionSave();

        $this->assertEquals(200, $result->statusCode());
        $this->assertEquals(['Ok'], $result->payload());
    }

    /**
     * @param mixed $expected
     * @param mixed $actual
     */
    private function assertEquals($expected, $actual): void
    {
        if ($expected != $actual) {
            throw new Exception('Unexpected result');
        };
    }
}

(new TestCase())->testSuccessful();
echo 'Successful' . PHP_EOL;